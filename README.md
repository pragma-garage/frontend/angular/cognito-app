# Cognito APP

Poyecto básico que ilustra de manera sencilla como realizar una autenticación y validación de usuarios utilizando el servicio AWS Cognito en conjunto con protección de rutas mediante el uso de Angular Guards.

## Configuración

Para realizar la configuración necesaria, se debe reemplazar los datos requeridos por la librería de Amplify, la cual será quien realize las conexiones necesarias con el servicio de AWS Cognito.

```
// auth.service.ts

Amplify.configure({
  "aws_project_region": "AWS_REGION",
  "aws_cognito_identity_pool_id": "AWS_COGNITO_IDENTITY_POOL_ID",
  "aws_cognito_region": "AWS_COGNITO_REGION",
  "aws_user_pools_id": "AWS_USER_POOLS_ID",
  "aws_user_pools_web_client_id": "AWS_USER_POOLS_WEB_CLIENT_ID"
});
```

Posteriormente es necesario instalar las dependencias necesarias de la aplicación mediante el siguiente comando:

```
npm install
```

## Servidor local

Para lanzar el proyecto en un servidor local solo basta con usar el siguiente comando.

```
ng serve
```

o

```
npm start
```