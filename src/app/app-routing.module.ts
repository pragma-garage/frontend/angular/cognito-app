import { AuthGuard } from './guards/auth.guard';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MODULES_ROUTES } from './common/routes';

const routes: Routes = [
  {
    path: MODULES_ROUTES.signIn,
    loadChildren: () => import('./signin/signin.module').then(m => m.SigninModule),
    canLoad: [AuthGuard]
  },
  {
    path: MODULES_ROUTES.signUp,
    loadChildren: () => import('./signup/signup.module').then(m => m.SignUpModule),
    canLoad: [AuthGuard]
  },
  {
    path: MODULES_ROUTES.home,
    loadChildren: () => import('./stuff/stuff.module').then(m => m.StuffModule),
    canLoad: [AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
